from django.contrib.gis.admin import OSMGeoAdmin


class PanelAdmin(OSMGeoAdmin):
    list_display        = ('code', 'position', 'type', 'format', 'verified')
    list_editable       = ('position', 'type', 'format')
    search_fields       = ('code', 'type', 'format', 'verified')
    list_per_page       = 10
    ordering            = ('code',)
    save_as             = True
    list_select_related = True
    fieldsets = (
        ('Informations'        , {'fields': ['code', 'type', 'format', ], 'classes': ('show', 'extrapretty')}),
        ('Position Geomertique', {'fields': ['position'              , ], 'classes': ('show', 'wide'       )}),
    )
    scrollable = False
    map_width  = 400
    map_height = 325
