from django.contrib.gis.admin import OSMGeoAdmin


class LocationAdmin(OSMGeoAdmin):
    list_display        = ('position',)
    list_per_page       = 10
    ordering            = ('position',)
    save_as             = True
    list_select_related = True
    fieldsets = (
        ('Position Geomertique', {'fields': ['position', ], 'classes': ('show', 'wide')}),
    )
    scrollable = False
    map_width  = 400
    map_height = 325
