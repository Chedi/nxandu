from django.contrib import admin

from api.models      import Event
from api.models      import Panel
from api.models      import Poster
from api.models      import Client
from api.models      import Device
from api.models      import Campaign
from api.models      import PanelType
from api.models      import PanelFormat
from api.admin.panel import PanelAdmin
from api.admin.event import EventAdmin


admin.site.register(Event       , EventAdmin)
admin.site.register(Panel       , PanelAdmin)
admin.site.register(Poster                  )
admin.site.register(Client                  )
admin.site.register(Device                  )
admin.site.register(Campaign                )
admin.site.register(PanelType               )
admin.site.register(PanelFormat             )
