from django.contrib.gis.admin import OSMGeoAdmin


class EventAdmin(OSMGeoAdmin):
    list_display        = ('poster', 'campaign', 'time', 'position', 'image_container')
    list_editable       = ('campaign', 'position')
    search_fields       = ('poster', 'campaign')
    list_per_page       = 10
    ordering            = ('time', 'campaign', 'poster')
    save_as             = True
    list_select_related = True
    fieldsets           = (
        ('Informations'        , {'fields': ['time', 'poster', 'campaign', 'panel', ], 'classes' : ('show', 'extrapretty')}),
        ('Image'               , {'fields': ['image_hash', 'image'                , ], 'classes' : ('show', 'wide'       )}),
        ('Position Geomertique', {'fields': ['position'                           , ], 'classes' : ('show', 'wide'       )}),
    )
    scrollable = False
    map_width  = 400
    map_height = 325
