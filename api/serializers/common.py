from rest_framework.serializers import ModelSerializer

from api.models import Event
from api.models import Panel
from api.models import Poster
from api.models import Client
from api.models import Device
from api.models import Campaign
from api.models import PanelType
from api.models import PanelFormat


class EventSerializer(ModelSerializer):
    class Meta:
        model = Event


class PanelSerializer(ModelSerializer):
    class Meta:
        model = Panel


class PosterSerializer(ModelSerializer):
    class Meta:
        model = Poster


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client


class DeviceSerializer(ModelSerializer):
    class Meta:
        model = Device


class CampaignSerializer(ModelSerializer):
    class Meta:
        model = Campaign


class PanelTypeSerializer(ModelSerializer):
    class Meta:
        model = PanelType


class PanelFormatSerializer(ModelSerializer):
    class Meta:
        model = PanelFormat
