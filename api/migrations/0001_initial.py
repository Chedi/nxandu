# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('photologue', '0006_auto_20141028_2005'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.TextField()),
                ('publie', models.BooleanField()),
                ('endDate', models.DateField()),
                ('startDate', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('raison_sociale', models.TextField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('serial', models.CharField(unique=True, max_length=30)),
                ('label', models.CharField(unique=True, max_length=20)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateField()),
                ('position', django.contrib.gis.db.models.fields.PointField(srid=3857)),
                ('image_hash', models.CharField(unique=True, max_length=256)),
                ('campaign', models.ForeignKey(to='api.Campaign')),
                ('image', models.ForeignKey(to='photologue.Photo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Panel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=30)),
                ('position', django.contrib.gis.db.models.fields.PointField(srid=3857)),
                ('verified', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PanelFormat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(unique=True, max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PanelType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(unique=True, max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Poster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('telephones', models.CharField(unique=True, max_length=20)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='panel',
            name='format',
            field=models.ForeignKey(to='api.PanelFormat'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='panel',
            name='type',
            field=models.ForeignKey(to='api.PanelType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='panel',
            field=models.ForeignKey(to='api.Panel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='poster',
            field=models.ForeignKey(to='api.Poster'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='campaign',
            name='client',
            field=models.ForeignKey(to='api.Client'),
            preserve_default=True,
        ),
    ]
