# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20150102_1754'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaign',
            name='client',
            field=models.ForeignKey(verbose_name='client', to='api.Client'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='campaign',
            name='endDate',
            field=models.DateField(verbose_name='end date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='campaign',
            name='label',
            field=models.TextField(unique=True, verbose_name='label'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='campaign',
            name='publie',
            field=models.BooleanField(default=True, verbose_name='publie'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='campaign',
            name='startDate',
            field=models.DateField(verbose_name='start date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='client',
            name='raison_sociale',
            field=models.TextField(verbose_name='raison sociale'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='client',
            name='user',
            field=models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL, unique=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='device',
            name='label',
            field=models.CharField(unique=True, max_length=20, verbose_name='label'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='device',
            name='serial',
            field=models.CharField(unique=True, max_length=30, verbose_name='serial'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='device',
            name='user',
            field=models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL, unique=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='campaign',
            field=models.ForeignKey(verbose_name='campaign', to='api.Campaign'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='image',
            field=models.ForeignKey(verbose_name='image', to='photologue.Photo'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='image_hash',
            field=models.CharField(unique=True, max_length=256, verbose_name='image hash'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='panel',
            field=models.ForeignKey(verbose_name='panel', to='api.Panel'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='position',
            field=django.contrib.gis.db.models.fields.PointField(srid=3857, verbose_name='position'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='poster',
            field=models.ForeignKey(verbose_name='poster', to='api.Poster'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='time',
            field=models.DateField(verbose_name='time'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='panel',
            name='code',
            field=models.CharField(unique=True, max_length=30, verbose_name='code'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='panel',
            name='format',
            field=models.ForeignKey(verbose_name='format', to='api.PanelFormat'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='panel',
            name='position',
            field=django.contrib.gis.db.models.fields.PointField(srid=3857, verbose_name='position'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='panel',
            name='type',
            field=models.ForeignKey(verbose_name='type', to='api.PanelType'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='panel',
            name='verified',
            field=models.BooleanField(default=False, verbose_name='verified'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='panelformat',
            name='label',
            field=models.CharField(unique=True, max_length=50, verbose_name='label'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='paneltype',
            name='label',
            field=models.CharField(unique=True, max_length=50, verbose_name='label'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='poster',
            name='telephones',
            field=models.CharField(unique=True, max_length=20, verbose_name='telephones'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='poster',
            name='user',
            field=models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL, unique=True),
            preserve_default=True,
        ),
    ]
