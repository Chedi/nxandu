from rest_framework.viewsets import ModelViewSet

from api.models      import Event
from api.models      import Panel
from api.models      import Poster
from api.models      import Client
from api.models      import Device
from api.models      import Campaign
from api.models      import PanelType
from api.models      import PanelFormat
from api.serializers import EventSerializer
from api.serializers import PanelSerializer
from api.serializers import PosterSerializer
from api.serializers import ClientSerializer
from api.serializers import DeviceSerializer
from api.serializers import CampaignSerializer
from api.serializers import PanelTypeSerializer
from api.serializers import PanelFormatSerializer


class EventViewSet(ModelViewSet):
    queryset         = Event.objects.all()
    serializer_class = EventSerializer


class PanelViewSet(ModelViewSet):
    queryset         = Panel.objects.all()
    serializer_class = PanelSerializer


class PosterViewSet(ModelViewSet):
    queryset         = Poster.objects.all()
    serializer_class = PosterSerializer


class ClientViewSet(ModelViewSet):
    queryset         = Client.objects.all()
    serializer_class = ClientSerializer


class DeviceViewSet(ModelViewSet):
    queryset         = Device.objects.all()
    serializer_class = DeviceSerializer


class CampaignViewSet(ModelViewSet):
    queryset         = Campaign.objects.all()
    serializer_class = CampaignSerializer


class PanelTypeViewSet(ModelViewSet):
    queryset         = PanelType.objects.all()
    serializer_class = PanelTypeSerializer


class PanelFormatViewSet(ModelViewSet):
    queryset         = PanelFormat.objects.all()
    serializer_class = PanelFormatSerializer


def init_routes(router):
    router.register(r'events'      , EventViewSet      )
    router.register(r'panels'      , PanelViewSet      )
    router.register(r'posters'     , PosterViewSet     )
    router.register(r'clients'     , ClientViewSet     )
    router.register(r'devices'     , DeviceViewSet     )
    router.register(r'campaigns'   , CampaignViewSet   )
    router.register(r'panel_type'  , PanelTypeViewSet  )
    router.register(r'panel_format', PanelFormatViewSet)
