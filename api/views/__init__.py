from api.views.login  import main
from api.views.login  import profile
from api.views.common import init_routes


__all__ = [
    'main'       ,
    'profile'    ,
    'init_routes',
]
