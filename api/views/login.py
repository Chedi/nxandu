from django.template                import RequestContext
from django.shortcuts               import render_to_response
from django.views.decorators.csrf   import csrf_protect
from django.contrib.auth.decorators import login_required


@csrf_protect
@login_required(login_url='/login/')
def profile(request):
    return render_to_response(
        'main.html',
        {},
        context_instance=RequestContext(request)
    )


@csrf_protect
@login_required(login_url='/login/')
def main(request):
    return render_to_response(
        'main.html',
        {},
        context_instance=RequestContext(request)
    )
