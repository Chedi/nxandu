from photologue.models          import Photo
from django.contrib.gis.db      import models
from django.core.exceptions     import ValidationError
from django.contrib.auth.models import User

from django.utils.translation import gettext_lazy as _


class PanelType(models.Model):
    label = models.CharField(_('label'), max_length=50, unique=True)

    def __unicode__(self):
        return self.label


class PanelFormat(models.Model):
    label = models.CharField(_('label'), max_length=50, unique=True)

    def __unicode__(self):
        return self.label


class Panel(models.Model):
    code     = models.CharField   (_('code'    ), max_length=30, unique=True)
    position = models.PointField  (_('position'), srid=3857                 )
    verified = models.BooleanField(_('verified'), default=False             )

    type   = models.ForeignKey   (PanelType, verbose_name=_('type')    )
    format = models.ForeignKey   (PanelFormat, verbose_name=_('format'))

    def __unicode__(self):
        return self.code


class Device(models.Model):
    label  = models.CharField (_('label' ), max_length=20, unique=True)
    serial = models.CharField (_('serial'), max_length=30, unique=True)

    user   = models.ForeignKey(User, unique=True, verbose_name=_('user'))

    def __unicode__(self):
        return u"{}".format(self.label)


class Poster(models.Model):
    user       = models.ForeignKey(User, unique=True, verbose_name=_('user'))
    telephones = models.CharField (_('telephones'), max_length=20, unique=True)

    def __unicode__(self):
        return u"{}".format(self.user.username)


class Client(models.Model):
    raison_sociale = models.TextField (_('raison sociale'))
    user           = models.ForeignKey(User, unique=True, verbose_name=_('user'))

    def __unicode__(self):
        return self.raison_sociale


class Campaign(models.Model):
    label     = models.TextField   (_('label' ), unique=True)
    publie    = models.BooleanField(_('publie'), default=True)
    endDate   = models.DateField   (_('end date'  ))
    startDate = models.DateField   (_('start date'))

    client = models.ForeignKey(Client, verbose_name=_('client'))

    def __unicode__(self):
        return self.label

    def clean(self):
        if self.startDate > self.endDate:
            raise ValidationError('end date must not precede start date')


class Event(models.Model):
    time       = models.DateField (_('time'))
    position   = models.PointField(_('position'), srid=3857)
    image_hash = models.CharField (_('image hash'), max_length=256, unique=True)

    image    = models.ForeignKey(Photo   , verbose_name=_('image'   ))
    panel    = models.ForeignKey(Panel   , verbose_name=_('panel'   ))
    poster   = models.ForeignKey(Poster  , verbose_name=_('poster'  ))
    campaign = models.ForeignKey(Campaign, verbose_name=_('campaign'))

    objects = models.GeoManager()

    def __unicode__(self):
        return u"({}, {})[{}]({} : {})".format(self.position.x, self.position.y, self.campaign, self.time, self.poster)

    def coordinates(self):
        return [self.position.x, self.position.y]

    def image_container(self):
        return self.image.admin_thumbnail()

    image_container.allow_tags = True
