var mapPanel   ;
var select_ctrl;
var current_selected_campaign;

var map_option = {
	units            : "m",
	maxExtent        : new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508.34),
	projection       : new OpenLayers.Projection("EPSG:900913"),
	numZoomLevels    : 18,
	maxResolution    : 156543.0339,
	displayProjection: new OpenLayers.Projection("EPSG:4326"),
};

var map                    = new OpenLayers.Map(map_option);
var googleLayer            = new OpenLayers.Layer.Google("Google Streets", {'sphericalMercator': true});
var select_controls_assoc  = new Array();
var campaigns_layers_assoc = new Array();
var campaigns_layers_norml = new Array();

var campaigns_loader = new Ext.tree.TreeLoader({
	url           : "/xanadu/api/campaigns/",
	requestMethod : 'GET',
});

var map_style = new OpenLayers.StyleMap({
	"default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
		externalGraphic: STATIC_URL + "/images/map/pin_blue_2.svg",
		rotation      : 0,
		pointRadius   : 15,
		graphicOpacity: 1,
	}, OpenLayers.Feature.Vector.style["default"])),
	"select": new OpenLayers.Style({
		externalGraphic: STATIC_URL + "/images/map/pin_brown_2.svg"
})});

Ext.onReady(function() {
	campaigns_loader.on("load", function(treeLoader, node, response) {
	if(node.attributes.id == "0") {
		for(i=0;i < node.childNodes.length;i++) {
			campaign = node.childNodes[i];
			current_events_layer = new OpenLayers.Layer.Vector(campaign.text, {
				renderers: ['Canvas', 'SVG', 'VML'],
				styleMap: map_style,
			});
			current_layer_features = new Array();
			for(j=0;j < campaign.attributes.children.length; j++){
				event = campaign.attributes.children[j];
				current_layer_features.push(
					new OpenLayers.Feature.Vector(
						new OpenLayers.Geometry.Point(event.position_x, event.position_y), {
							id          : event.id,
							date        : event.text,
							image       : event.image,
							poster      : event.poster,
							campaign    : event.campaign,
							campaign_id : campaign.attributes.id,
						}, null));
			}
			current_events_layer.addFeatures(current_layer_features);
			current_events_layer.events.on({
				featureselected: function(e) {
					createPopup(e.feature);
			}});
			current_select_ctrl = new OpenLayers.Control.SelectFeature(current_events_layer);
			mapPanel.map.addControl(current_select_ctrl);
			current_select_ctrl.activate();
			campaigns_layers_norml.push(current_events_layer);
			select_controls_assoc [campaign.attributes.id] = current_select_ctrl ;
			campaigns_layers_assoc[campaign.attributes.id] = current_events_layer;
		}
		map.addLayers(campaigns_layers_norml);
	}});

	map.addLayers([googleLayer]);

	new Ext.Viewport({
		layout: "border",
		items: [{
			id    : "mappanel",
			map   : map,
			zoom  : 8,
			title : "Map",
			xtype : "gx_mappanel",
			split : true,
			region: "center",
			center: new OpenLayers.LonLat(971064.742483, 4270114.1624798),
		}, {
			region: 'west',
			collapsible: true,
			title: 'Campaigns',
			xtype: 'treepanel',
			width: 200,
			autoScroll: true,
			split: true,
			expanded: true,
			loader: campaigns_loader,
			root: {
				nodeType: 'async',
				text: 'Campaigns',
				draggable: false,
				id: "0",
			},
			rootVisible: true,
			listeners: {
				click: function(n) {
					if(n.isRoot){
						for(i=0;i < campaigns_layers_norml.length;i++){
							campaigns_layers_norml[i].setVisibility(true);
					}} else if(!n.leaf){
						for(i=0;i < campaigns_layers_norml.length;i++){
							campaigns_layers_norml[i].setVisibility(false);
						}
						campaigns_layers_assoc[n.attributes.id].setVisibility(true);
					} else {
						layer_features = campaigns_layers_assoc[n.attributes.campaign_id].features;
						for(i=0; i < layer_features.length;i++){
							if(layer_features[i].attributes.id = n.attributes.id){
								select_controls_assoc[n.attributes.campaign_id].select(layer_features[i]);
								break;
					}}}
	}}}]});

	mapPanel = Ext.getCmp("mappanel");

	function createPopup(feature) {
		current_selected_campaign = feature.data.campaign_id;
		var event_detail =
			"Campaign name : " + feature.data.campaign + "<br/>"+
			"Poster : "        + feature.data.poster   + "<br/>"+
			"date : "          + feature.data.date     + "<br/>"+
			"<image src='"     + feature.data.image    +"'>";
		popup = new GeoExt.Popup({
			title      : 'Campaign Details',
			location   : feature,
			width      : 500,
			html       : event_detail,
			maximizable: false,
			collapsible: false,
		});

		popup.on({
			close: function() {
				select_controls_assoc[current_selected_campaign].unselectAll();
		}});
		popup.show();
	}
});
