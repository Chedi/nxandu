Ext.define('CampaignViewer.App', {
	extend: 'Ext.container.Viewport',
	initComponent: function(){
		Ext.apply(this, {
			layout: 'border',
			padding: 5,
			items: [this.createCampaignPanel(), this.createCampaignMap()]
		});
		this.callParent(arguments);
	},

	createCampaignPanel: function(){
		this.campaign_panel = Ext.create('widget.CampaignPanel', {
			split      : true,
			width      : 225,
			region     : 'west',
			minWidth   : 175,
			floatable  : false,
			campaigns  : campaigns,
			collapsible: true,
			listeners: {
				scope: this,
				campaign_select: this.onCampaignSelect,
			}
		});
		return this.requestPanel;
	},

	createCampaignMap: function(){
		this.campaign_map = Ext.create('widget.CampaignMap', {
			region: 'center',
			minWidth: 300
		});
		return this.campaign_map;
	},

	onCampaignSelect: function(request, id, title){
	},
});