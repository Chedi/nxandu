Ext.define('CampaignViewer.CampaignMap', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.CampaignMap',
	maxTabWidth: 230,
	border: false,
	
	initComponent: function() {
		this.callParent(arguments);
		this.on('afterrender', this.afterRender, this);
	},

	afterRender: function() {
		var wh = this.ownerCt.getSize();
		Ext.applyIf(this, wh);
		this.callParent(arguments);

		this.map   = new OpenLayers.Map(this.body.dom.id);
		this.layer = new OpenLayers.Layer.OSM('OSM Map');
		this.layer.setIsBaseLayer(true);
		this.map.addLayer(this.layer);
	}
});
