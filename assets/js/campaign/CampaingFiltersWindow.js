Ext.define('CampaignViewer.CampaingFiltersWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.CampaingFiltersWindow',
	plain: true,

	initComponent: function(){
		this.addEvents('requestvalid');
		this.form = Ext.create('widget.form', {
			bodyPadding: '12 10 10',
			border: false,
			unstyled: true,
			items:
			[
				{
					anchor: '100%',
					itemId: 'request_description',
					fieldLabel: 'Enter the request description',
					labelAlign: 'top',
					msgTarget: 'under',
					xtype: 'textareafield',
					grow : true,
				},
				{
					itemId : 'request_field',
					fieldLabel  : 'training field',
					store       : all_training_fields,
					displayField: 'label',
					valueField  : 'id',
					xtype: 'combo',
					anchor: '100%',
				}
			]
		});
		Ext.apply(this, {
			width: 500,
			title: 'Add Request',
			iconCls: 'request',
			layout: 'fit',
			items: this.form,
			buttons: [{
				xtype: 'button',
				text: 'Add Request',
				scope: this,
				handler: this.onAddClick
			}, {
				xtype: 'button',
				text: 'Cancel',
				scope: this,
				handler: this.destroy
			}]
		});
		this.callParent(arguments);
	},

	onAddClick: function(){
		var description = this.form.getComponent('request_description').getValue();
		var field       = this.form.getComponent('request_field').getValue();

		this.form.setLoading({
			msg: 'Validating request...'
		});
		
		Ext.Ajax.defaultHeaders = {
			'Accept': 'application/json'
		};

		Ext.Ajax.request({
			method : 'POST',
			url: '/training/api/requests/',
			jsonData: {
				description : description,
				field       : field,
			},
			success: this.validateRequest,
			failure: this.markInvalid,
			scope: this
		});
	},

	validateRequest: function(response){
		this.form.setLoading(false);
		try {
			request = eval('('+ response.responseText +')')
			request_id = request.id;
			if (request_id) {
				requests.load();
				this.fireEvent('requestvalid', this, request);
				this.destroy();
				return;
			}
		} catch(e) {
		}
	},

	markInvalid: function(){
		this.form.setLoading(false);
		this.form.getComponent('request').markInvalid('error.');
	}
});
