from os.path import join
from os.path import abspath
from os.path import dirname

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

BASE_DIR = dirname(dirname(__file__))

def getpath(resource_path):
    return abspath(join(BASE_DIR, resource_path))

SECRET_KEY = 'ch01t68vwx71z+s01(5f3n7*a&puozkel-bc&s-a09+0)q2&9$'

DEBUG           = True
SITE_ID         = 1
TEMPLATE_DEBUG  = True
POSTGIS_VERSION = (2, 1, 5)

ALLOWED_HOSTS = []


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.gis',

    'api',
    'suit',
    'djangular',
    'photologue',
    'djangobower',
    'rest_framework',
    'django_extensions',

    'django.contrib.admin',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF     = 'xanadu.urls'
WSGI_APPLICATION = 'xanadu.wsgi.application'

DATABASES = {
    'default': {
        'NAME'    : 'xanadu',
        'ENGINE'  : 'django.contrib.gis.db.backends.postgis',
        'USER'    : 'postgres',
        'PORT'    : '5432',
        'HOST'    : '127.0.0.1',
        'PASSWORD': '',
    }
}

USE_TZ        = True
USE_I18N      = True
USE_L10N      = True
TIME_ZONE     = 'UTC'
LANGUAGE_CODE = 'en-us'


MEDIA_URL  = '/media/'
STATIC_URL = '/static/'

MEDIA_ROOT  = getpath('../media' )
STATIC_ROOT = getpath('../static')

LOCALE_PATHS          = (getpath('locale'   ),)
TEMPLATE_DIRS         = (getpath('templates'),)
STATICFILES_DIRS      = (getpath('assets'   ),)
BOWER_COMPONENTS_ROOT = getpath('components')


BOWER_INSTALLED_APPS = (
    'Polymer/polymer',
    'Polymer/core-elements',
    'Polymer/paper-elements'
)


STATICFILES_FINDERS = (
    'djangobower.finders.BowerFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder')

TEMPLATE_LOADERS = (('django.template.loaders.cached.Loader', (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader'
)),)

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
)
