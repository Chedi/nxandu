from django.contrib   import admin
from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls import patterns

from rest_framework.routers import DefaultRouter

from api.views import main
from api.views import profile
from api.views import init_routes as api_init_routes

router = DefaultRouter()
api_init_routes(router)


urlpatterns = patterns('',
    url(r'^$'                 , profile),
    url(r'^main/'             , main   ),
    url(r'^admin/'            , include(admin.site.urls)),
    url(r'^login/?$'          , 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^photologue/'       , include('photologue.urls')),
    url(r'^api/api-auth/'     , include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/profile/$', profile),
) + [url(r"^api/" + i.regex.pattern[1:], i.callback, name=i.name) for i in router.urls]
